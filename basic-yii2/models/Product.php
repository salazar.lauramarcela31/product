<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $name
 * @property int $user_id
 * @property string $date
 * @property int $product_category_id
 * @property int $amount_points
 * @property string $instructions_use
 * @property string $details
 * @property string $terms
 * @property string $image
 * @property int $status
 *
 * @property ProductCategory $productCategory
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'user_id', 'date', 'product_category_id', 'amount_points', 'instructions_use', 'details', 'terms', 'image'], 'required'],
            [['user_id', 'product_category_id', 'amount_points', 'status'], 'integer'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 60],
            [['instructions_use', 'details'], 'string', 'max' => 300],
            [['terms'], 'string', 'max' => 200],
            [['image'], 'string', 'max' => 100],
            [['image'], 'file', 'extensions' => 'jpg,jpeg,png'],
            [['product_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::class, 'targetAttribute' => ['product_category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'user_id' => Yii::t('app', 'User ID'),
            'date' => Yii::t('app', 'Date'),
            'product_category_id' => Yii::t('app', 'Product Category ID'),
            'amount_points' => Yii::t('app', 'Amount Points'),
            'instructions_use' => Yii::t('app', 'Instructions Use'),
            'details' => Yii::t('app', 'Details'),
            'terms' => Yii::t('app', 'Terms'),
            'image' => Yii::t('app', 'Image'),
            'status' => Yii::t('app', 'Status'),
        ];
    }

    /**
     * Gets query for [[ProductCategory]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategory()
    {
        return $this->hasOne(ProductCategory::class, ['id' => 'product_category_id']);
    }
}
